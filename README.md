# Cloud Drive

Host your own cloud with web interface and client sync.
This is the backend application of the project written in Rust. The server framework used is actix.


## Getting started

To run it locally, make sure the rust toolchain is installed, clone the repo and run `cargo run`.


## Features
- User Management
- File Browsing, Download and Upload
- (more to come)


## Support
For support open issues in this repo.

## Roadmap
- Richer file api
- Sync API for desktop clients

## Contributing
If you want to contribute, create a merge request. Make sure your code is approved by `clippy`.

## Authors and acknowledgment
- https://gitlab.com/jonry
- https://gitlab.com/davidschnrr

## License
MIT License

## Project status
Status: Inactive

Working on it during free time.
