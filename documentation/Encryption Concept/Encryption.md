# Encrytion (Plans)
Plans are to implement encryption in a way which gives the user 3 choices. Encryption will be E2E. The files will be encrypted & decrypted on the client in all three scenarios. 

## Encryption with a password
This method will enable the user to set an encryption password for a file. This password will be hashed and the hash used as the AES encryption key.
If the user needs to read the file from the server or wants to share it with a different user the orignal password is needed.

## Encryption with a generated key
The client will generate a secure AES key which the user needs to download. It is on the user to manage the key. If a file needs to be shared with a different user
the AES key has to be shared.

## Encryption with RSA encrypted AES key
In this scenario the client will generate an AES key and a RSA key pair. The AES key will be encrypted with the public key and store on the server together with the public key.
The user needs to take care of the private key which is used to decrypt the AES key and restore a file.
Sharing works as follows. As an example Alice wants to share a file with Bob. Alice gets Bob's public key and her encrypted AES key from the server. She decrypts the AES key with her
private key and encryptes it with Bob's public key. Bob's AES key will be stored next to Alice'. Now Bob can read the file, since he can decrypt the AES key with his private key.

Detail flow chart are below. All CRUD actions on Cloud drive server are done on a seperated DB. 

Create User:

![This is an image](./Create_New_Account/CreateUser.jpg)

Upload File encrypted:

![This is an image](./Upload_File/UploadFile.jpg)

Share Encrypted File:

![This is an image](./Share_encrypted_file/ShareEncryptedFile.jpg)



Diagrams created with https://app.diagrams.net
