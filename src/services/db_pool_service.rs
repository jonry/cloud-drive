use std::env;
use diesel::sqlite::SqliteConnection;
use r2d2_diesel::{ConnectionManager};

use dotenv::dotenv;

use r2d2::{Pool, PooledConnection};
use crate::services::user_management::sqlite_service;


pub type SqlPool = Pool<ConnectionManager<SqliteConnection>>;
pub type SqlPooledConnection = PooledConnection<ConnectionManager<SqliteConnection>>;


//Creates a default R2D2 Postgres DB Pool
fn init_pool(database_url: &str) -> Result<SqlPool, String> {
    let manager = ConnectionManager::<SqliteConnection>::new(database_url);
    let pool = match Pool::builder().build(manager) {
        Ok(pool) => pool,
        Err(e) => return Err(e.to_string())
    };

    match sqlite_service::init_empty_db(&pool) {
        Ok(_) => Ok(pool),
        Err(e) => Err(e)
    }
}

pub fn establish_connection() -> SqlPool {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    init_pool(&database_url).expect("Failed to create pool")
}

pub fn get_connection_from_pool(pool: &SqlPool) -> Result<SqlPooledConnection, r2d2::Error> {
    pool.get()
}


// ------------- Example for optimization later on -----------------
/*  r2d2_diesel::Error

#[derive(Debug)]
pub struct ConnectionOptions {
    pub enable_wal: bool,
    pub enable_foreign_keys: bool,
    pub busy_timeout: Option<Duration>,
}

impl CustomizeConnection<SqliteConnection, Error>
for ConnectionOptions
{
    fn on_acquire(&self, conn: &mut SqliteConnection) -> Result<(), Error> {
        (|| {
            if self.enable_wal {
                conn.batch_execute("PRAGMA journal_mode = WAL; PRAGMA synchronous = NORMAL;")?;
            }
            if self.enable_foreign_keys {
                conn.batch_execute("PRAGMA foreign_keys = ON;")?;
            }
            if let Some(d) = self.busy_timeout {
                conn.batch_execute(&format!("PRAGMA busy_timeout = {};", d.as_millis()))?;
            }
            Ok(())
        })()
            .map_err(Error::QueryError)
    }
}

*/