use diesel::{QueryDsl, RunQueryDsl, OptionalExtension, TextExpressionMethods};
use crate::db_pool_service::{get_connection_from_pool, SqlPool};
use crate::models::user_management::roles::Role;
use crate::models::user_management::user::User;
use crate::schema::users;
use crate::schema::users::dsl::*;

pub fn insert_new_user(pool: &SqlPool, user: User) -> Result<User, String> {
    let connection = match get_connection_from_pool(pool) {
        Ok(conn) => conn,
        Err(e) => return Err(e.to_string())
    };

    match diesel::insert_into(users::table)
        .values(&user)
        .execute(&*connection) {
        Ok(_) => Ok(user),
        Err(e) => Err(e.to_string())
    }
}

pub fn get_user(pool: &SqlPool, user_name: &str) -> Result<User, String> {
    let connection = match get_connection_from_pool(pool) {
        Ok(conn) => conn,
        Err(e) => return Err(e.to_string())
    };

    match users.limit(1)
        .filter(username.like(user_name))
        .get_result::<User>(&*connection)
        .optional() {
        Ok(user) => match user {
            Some(user) => Ok(user),
            None => Err("User not found".to_string())
        },
        Err(err) => Err(err.to_string())
    }
}


pub fn init_empty_db(pool: &SqlPool) -> Result<(), String> {
    let connection = match get_connection_from_pool(pool) {
        Ok(conn) => conn,
        Err(e) => return Err(e.to_string())
    };

    let count = match users.count().get_result::<i64>(&*connection) {
        Ok(count) => count,
        Err(_) => {
            let query = "CREATE TABLE users ( username VARCHAR NOT NULL PRIMARY KEY,email VARCHAR NOT NULL,password_hash VARCHAR NOT NULL,user_salt VARCHAR NOT NULL,quota BIGINT NOT NULL,role TEXT NOT NULL)";
            match diesel::sql_query(query).execute(&*connection) {
                Ok(_) => {}
                Err(e) => return Err(e.to_string())
            };
            0
        }
    };

    if count != 0 {
        return Ok(());
    }

    let user = User {
        username: "admin".to_string(),
        email: "".to_string(),
        password_hash: "9e71123ed621379fb2924f29b80de7f9b52c58be108d9cf5c202a21319f965508bb84a42a5fdf5860650f2fd1ad27432497a2ab7ab54ca67e19bb9f26d08d3d2".to_string(),
        user_salt: "rDIQsh02q1FaHxi2".to_string(),
        quota: 10,
        role: Role::Owner,
    };

    match diesel::insert_into(users::table)
        .values(&user)
        .execute(&*connection) {
        Ok(_) => Ok(()),
        Err(e) => Err(e.to_string())
    }
}