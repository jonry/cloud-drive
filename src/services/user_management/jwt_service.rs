use actix_web::http::header::HeaderValue;
use crate::models::user_management::claim::Claims;
use crate::models::user_management::user::User;
use chrono::{Duration, Utc};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use jsonwebtoken::errors::ErrorKind;

use crate::models::user_management::error::jwt_error::JwtError;

pub fn generate_token(user: &User, secret: &str) -> Result<String, jsonwebtoken::errors::Error> {
    let secret = secret.as_bytes();

    let expires_at = (Utc::now() + Duration::minutes(15)).timestamp() as usize;

    let claims = Claims {
        sub: user.username.clone(),
        role: user.role,
        exp: expires_at,
    };
    match encode(&Header::default(), &claims, &EncodingKey::from_secret(secret)) {
        Ok(token) => Ok(token),
        Err(er) => Err(er)
    }
}

pub fn get_claim_from_token(jwt: &HeaderValue, secret: &str) -> Result<Claims, JwtError> {
    let secret = secret.as_bytes();
    if let Ok(jwt) = jwt.to_str() {
        let token_data =
            match decode::<Claims>(jwt, &DecodingKey::from_secret(secret), &Validation::default()) {
                Ok(c) => c,
                Err(err) => return match err.kind() {
                    // TODO: adapt
                    ErrorKind::ExpiredSignature => Err(JwtError::Expired), // Example on how to handle a specific error
                    _ => Err(JwtError::Invalid),
                },
            };
        // Todo: include this validation somehow in the JWT validation. Customize Validation somehow? see jsonwebtoken::Validation

        Ok(token_data.claims)
    } else {
        Err(JwtError::Invalid)
    }
}



/*

impl Role{
    fn is_valid(&self, required_role: &Role) -> bool{
        match self{
            Role::Owner => true,
            Role::Admin => required_role != &Role::Owner,
            Role::User => required_role == &Role::User
        }
    }
}

*/