use rand::Rng;
use crypto::digest::Digest;
use crypto::sha3::Sha3;
use rand::distributions::Alphanumeric;
use crate::db_pool_service::SqlPool;
use crate::models::user_management::error::sign_in_error::SignInError;
use crate::models::user_management::requests::create_user::CreateUserRequest;
use crate::models::user_management::requests::signin_request::SigninRequest;

use crate::models::user_management::user::User;
use crate::services::user_management::sqlite_service;
use crate::services::user_management::sqlite_service::get_user;


pub fn create_user(pool: &SqlPool, user_details: &CreateUserRequest) -> Result<User, String> {

    let salt: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(16)
        .map(char::from)
        .collect();
    let pass_word_hash= get_password_hash(&user_details.password, &salt);
    let user = User {
        username: user_details.username.clone(),
        email: user_details.email.clone(),
        password_hash: pass_word_hash,
        user_salt: salt,
        quota: user_details.quota,
        role: user_details.role,
    };

    sqlite_service::insert_new_user(pool, user)
}

fn get_password_hash(password: & str, salt: &str) -> String {

    let password_and_hash = format!("{}{}", password, salt);
    let mut hasher = Sha3::sha3_512();
    hasher.input(password_and_hash.as_bytes());
    hasher.result_str()

}

pub fn authenticate_user(sign_request: SigninRequest, pool: &SqlPool) -> Result<User, SignInError>{
    if let Ok(user) = get_user(pool, &sign_request.username){
        if get_password_hash(&sign_request.password, &user.user_salt).eq(&user.password_hash){
           return Ok(user)
        }
       return Err(SignInError::PasswordIncorrect)
    }
    Err(SignInError::UserNotFound)
}
