use std::rc::Rc;
use actix_web::{FromRequest, HttpMessage, HttpRequest};
use actix_web::dev::Payload;
use futures::future::{Ready, ready};
use crate::models::user_management::claim::Claims;
use crate::models::user_management::error::jwt_error::JwtError;

use crate::models::user_management::roles::Role;


pub type AuthenticationInfo = Rc<Claims>;

fn user_has_needed_permissions(incoming: &Role, required: &Role) -> bool {
    match incoming {
        Role::Owner => true,
        Role::Admin => required != &Role::Owner,
        Role::User => required == &Role::User
    }
}

pub struct OwnerExtractor(pub AuthenticationInfo);

impl FromRequest for OwnerExtractor {
    type Error = JwtError;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(req: &actix_web::HttpRequest,
                    _payload: &mut actix_web::dev::Payload) -> Self::Future {
        let value = req.extensions().get::<AuthenticationInfo>().cloned();

        let extractor = match value {
            Some(v) => OwnerExtractor(v),
            None => return ready(Err(JwtError::Missing)),
        };

        if extractor.0.role == Role::Owner {
            ready(Ok(extractor))
        } else {
            ready(Err(JwtError::Invalid))
        }
    }
}

pub struct AdminExtractor(pub AuthenticationInfo);

impl FromRequest for AdminExtractor {
    type Error = JwtError;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut Payload) -> Self::Future {
        let value = req.extensions().get::<AuthenticationInfo>().cloned();

        let extractor = match value {
            Some(v) => AdminExtractor(v),
            None => return ready(Err(JwtError::Missing)),
        };

        if user_has_needed_permissions(&extractor.0.role, &Role::Admin) {
            ready(Ok(extractor))
        } else {
            ready(Err(JwtError::Invalid))
        }
    }
}

pub struct UserExtractor(pub AuthenticationInfo);

impl FromRequest for UserExtractor {
    type Error = JwtError;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut Payload) -> Self::Future {
        let value = req.extensions().get::<AuthenticationInfo>().cloned();


        let extractor = match value {
            Some(v) => UserExtractor(v),
            None => return ready(Err(JwtError::Missing)),
        };

        if user_has_needed_permissions(&extractor.0.role, &Role::Admin) {
            ready(Ok(extractor))
        } else {
            ready(Err(JwtError::Invalid))
        }
    }
}
