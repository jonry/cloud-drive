table! {
    use diesel::sql_types::*;
    use crate::models::user_management::roles::RoleMapping;

    users (username) {
        username -> Text,
        email -> Text,
        password_hash -> Text,
        user_salt -> Text,
        quota -> BigInt,
        role -> RoleMapping,
    }
}
