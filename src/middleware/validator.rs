use std::rc::Rc;
use std::sync::{Arc, RwLock};
use actix_service::{Service, Transform};
use actix_web::{dev::ServiceRequest, dev::ServiceResponse, Error, HttpMessage, web};
use futures::future::{LocalBoxFuture, Ready, ready};
use futures::{FutureExt};
use crate::models::user_management::claim::Claims;
use crate::services::user_management::jwt_service::{get_claim_from_token};


pub struct AuthenticateMiddleware<S> {
    service: Rc<S>,
}

// resources used for implementation: https://dev.to/dimfeld/demystifying-actix-web-middleware-3lef - https://github.com/dimfeld/ergo/tree/master/auth

impl<S, B> Service<ServiceRequest> for AuthenticateMiddleware<S>
    where
        S: Service<ServiceRequest, Response=ServiceResponse<B>, Error=Error> + 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;

    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    actix_service::forward_ready!(service);

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let srv = Rc::clone(&self.service);

        async move {
            if let Some(key) = req.headers().get("x-api-key") {
                let secret = match req.app_data::<web::Data<Arc<RwLock<String>>>>() {
                    Some(secret) => secret,
                    None => return Err(actix_web::error::ErrorInternalServerError("could not get internal secret"))
                };

                let secret = match secret.read() {
                    Ok(res) => res.clone(),
                    Err(e) => return Err(actix_web::error::ErrorInternalServerError(e.to_string()))
                };

                if let Ok(claim) = get_claim_from_token(key, &secret) {
                    req.extensions_mut()
                        .insert::<Rc<Claims>>(Rc::new(claim));
                }
            }

            let res = srv.call(req).await?;
            Ok(res)
        }.boxed_local()
    }
}

pub struct AuthenticateMiddlewareFactory {}

impl AuthenticateMiddlewareFactory {
    pub fn new() -> Self {
        AuthenticateMiddlewareFactory {}
    }
}

impl<S, B> Transform<S, ServiceRequest> for AuthenticateMiddlewareFactory
    where
        S: Service<ServiceRequest, Response=ServiceResponse<B>, Error=Error> + 'static,
{
    type Response = ServiceResponse<B>;

    type Error = Error;

    type Transform = AuthenticateMiddleware<S>;

    type InitError = ();

    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ready(Ok(AuthenticateMiddleware {
            service: Rc::new(service),
        }))
    }
}
