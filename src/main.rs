mod controllers;
mod guards;
mod models;
mod services;
mod schema;
mod middleware;

use std::sync::{Arc, RwLock};
use actix_web::{App, HttpServer, web};
use actix_web::middleware::Logger;
use actix_web::web::Data;
use controllers::account_controller::*;
use env_logger::Env;
use rand::distributions::Alphanumeric;
use rand::Rng;
use middleware::validator;
use crate::services::db_pool_service;
use crate::validator::{AuthenticateMiddlewareFactory};

#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate r2d2_diesel;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let secret: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(1024)
        .map(char::from)
        .collect();

    let secret_container = Arc::new(RwLock::new(secret.clone()));

    env_logger::init_from_env(Env::default().default_filter_or("info"));
    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(secret_container.clone()))
            .app_data(Data::new(db_pool_service::establish_connection()))
            .wrap(AuthenticateMiddlewareFactory::new())
            .service(
                web::scope("/api/account")
                    .service(sign_in)
                    .service(create)
                    .service(edit)
                    .service(delete)
                    .service(list)
                    .service(get_user_info)
                    .service(get_user_from_name)
            ).wrap(Logger::default())
    })
        .bind("127.0.0.1:8000")?
        .run()
        .await
}
