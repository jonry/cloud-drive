use std::sync::{Arc, RwLock};
use crate::models::user_management::user::{User};
use crate::schema::users::dsl::*;
use actix_web::{get, post, web, HttpResponse};
use actix_web::web::Json;
use diesel::{prelude::*};

use crate::guards::authentication_extractor::{AdminExtractor, UserExtractor};
use crate::models::user_management::requests::create_user::CreateUserRequest;
use crate::models::user_management::requests::signin_request::SigninRequest;

use crate::services::user_management::account_service;
use crate::services::user_management::account_service::authenticate_user;
use crate::services::db_pool_service::*;
use crate::services::user_management::jwt_service::generate_token;
use crate::services::user_management::sqlite_service::get_user;


#[post("/create")]
pub fn create(pool: web::Data<SqlPool>, user: web::Json<CreateUserRequest>, _: AdminExtractor) -> HttpResponse {
    match account_service::create_user(&pool, &user) {
        Ok(user) =>
            HttpResponse::Ok().json(user),
        Err(e) => HttpResponse::InternalServerError().json(e)
    }
}


#[post("/signin")]
pub async fn sign_in(pool: web::Data<SqlPool>, secret: web::Data<Arc<RwLock<String>>>, request: Json<SigninRequest>) -> HttpResponse {
    let res = match secret.read() {
        Ok(res) => res,
        Err(e) => return HttpResponse::InternalServerError()
            .json(format!("could not load internal secret: {}", e)),
    };

    match authenticate_user(request.0, &pool) {
        Ok(user) => match generate_token(&user, &res) {
            Ok(jwt) => HttpResponse::Ok().json(jwt),
            Err(_) => HttpResponse::Forbidden().json("error generating JWT")
        },
        Err(er) => HttpResponse::Forbidden().json(er)
    }
}

#[get("/edit")]
pub fn edit(_: UserExtractor) -> HttpResponse {
    HttpResponse::Ok().json("Edit user")
}

#[get("/delete")]
pub fn delete() -> HttpResponse {
    HttpResponse::Ok().json("delete user")
}

#[get("/list")]
pub fn list(pool: web::Data<SqlPool>, _: AdminExtractor) -> HttpResponse {
    let connection = get_connection_from_pool(&pool).unwrap();
    let results = users.limit(20)
        .load::<User>(&*connection)
        .expect("Error loading users");


    HttpResponse::Ok().json(results)
}

#[get("/user")]
pub fn get_user_info(pool: web::Data<SqlPool>, claim: UserExtractor) -> HttpResponse {
    match get_user(&pool, &claim.0.sub) {
        Ok(user) => HttpResponse::Ok().json(user),
        Err(err) => HttpResponse::InternalServerError().json(err)
    }
}

#[get("/user/{name}")]
pub fn get_user_from_name(pool: web::Data<SqlPool>, _: AdminExtractor, name: web::Path<String>) -> HttpResponse {
    match get_user(&pool, &name) {
        Ok(user) => HttpResponse::Ok().json(user),
        Err(err) => HttpResponse::InternalServerError().json(err)
    }
}