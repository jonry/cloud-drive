use diesel_derive_enum::DbEnum;
use strum_macros::EnumString;
use serde::{Deserialize, Serialize};


// Enum need to be fieldless for DbEnum

#[derive(Debug, Serialize, Deserialize, Copy, Clone, PartialEq, EnumString, strum_macros::ToString, DbEnum)]
pub enum Role {
    #[strum(serialize = "owner")]
    Owner,
    // = 0,
    #[strum(serialize = "admin")]
    Admin,
    // = 1,
    #[strum(serialize = "user")]
    User, //= 2,
}

