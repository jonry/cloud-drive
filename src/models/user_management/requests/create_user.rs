use crate::models::user_management::roles::Role;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Queryable, PartialEq)]
pub struct CreateUserRequest {
    pub username: String,
    pub email: String,
    pub password: String,
    pub quota: i64,
    pub role: Role,
}