use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct SigninRequest {
    pub username: String,
    pub password: String, 
 }