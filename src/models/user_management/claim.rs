use serde::{Deserialize, Serialize};

use crate::models::user_management::roles::Role;

#[derive(Debug, Serialize, Deserialize,Clone)]
pub struct Claims {
   pub sub: String,
   pub exp: usize,
  //  iat: usize,
   pub role: Role,

}