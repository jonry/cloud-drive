use serde::{ Serialize};

#[derive(Debug, Serialize, strum_macros::ToString)]
pub enum SignInError{
    #[strum(serialize = "user name not correct")]
    UserNotFound,
    #[strum(serialize = "password not correct")]
    PasswordIncorrect
}