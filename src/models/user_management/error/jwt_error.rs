use std::fmt;
use std::fmt::{Display};
use actix_web::{http::StatusCode, HttpResponse};

#[derive(Debug)]
pub enum JwtError {
    Missing,
    Invalid,
    Expired,
    //Authorisation,
}

impl Display for JwtError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl actix_web::error::ResponseError for JwtError {
    fn status_code(&self) -> StatusCode {
        match self {
            JwtError::Missing => StatusCode::BAD_REQUEST,
            JwtError::Expired => StatusCode::UNAUTHORIZED,
            JwtError::Invalid => StatusCode::UNAUTHORIZED,
            //JwtError::Authorisation => StatusCode::FORBIDDEN,
        }
    }
    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code()).json(self.to_string())
    }
}