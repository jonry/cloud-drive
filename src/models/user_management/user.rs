use serde::{Deserialize, Serialize};
use diesel::*;
use crate::schema::users;

use crate::models::user_management::roles::{Role};

#[derive(Insertable, Queryable, Serialize, Deserialize, PartialEq)]
pub struct User {
    pub username: String,
    pub email: String,
    pub password_hash: String,
    pub user_salt: String,
    pub quota: i64,
    pub role: Role,
}
