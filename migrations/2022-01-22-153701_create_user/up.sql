-- Your SQL goes here

CREATE TABLE users (
  username VARCHAR NOT NULL PRIMARY KEY,
  email VARCHAR NOT NULL,
  password_hash VARCHAR NOT NULL,
  user_salt VARCHAR NOT NULL,
  quota BIGINT NOT NULL,
  role TEXT NOT NULL
)
